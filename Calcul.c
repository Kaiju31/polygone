#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Points.h"

float Calcul(Point Poly1, Point Poly2, float total){
    int v_x = Poly2.x - Poly1.x;
    int v_y = Poly2.y - Poly1.y;
    total = sqrt(pow(v_x,2) + pow(v_y,2));
    return total;
}

float calcPerimetre(Point* Poly, int nb_points){
    float peri = 0;
    float total = 0;
    int i = 0;
    while(i<nb_points){
        peri += Calcul(Poly[i], Poly[i+1], total);
        i++;
    }
        peri += Calcul(Poly[5], Poly[0], total);
        printf("Le perimetre du polygone est %f", peri);
        return 0;
}




