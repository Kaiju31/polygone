#include <stdio.h>
#include <stdlib.h>
#include "Points.h"

int main(int argc, char *argv[])
{
    float perim = 0;
    Point Poly[6] = {{2,0},{6,0},{10,2},{8,6},{2,8},{0,4}};
    perim += calcPerimetre(Poly, 5);
    return 0;
}
