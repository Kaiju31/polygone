#ifndef POINTS_H_INCLUDED
#define POINTS_H_INCLUDED
typedef struct Point Point;

struct Point{
    int x;
    int y;
};
float calcPerimetre(Point* Poly, int nb_points);


#endif // POINTS_H_INCLUDED
